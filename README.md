Firmware for the ELysis DC v1.*

More information at : https://liothique.xyz/elysis


## Dev notes

Avoid using systick interrupt on core0 as I did not 
check that the core1 intializer copies the vector
table (it probably does since the stack ptr need to
be at offset 0 of that table, and it is not shared
by the core). We use core1 systick as time source since
the timer peripheral is used by RTIC on core0. (and I cant get
systick-monotonic to work for RTIC on core0 smdh)

core1 launch uses the FIFO: only send user value once
it is launched, however weird to debug problem
