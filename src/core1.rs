use crate::params::*;
use cortex_m::prelude::*;

use cortex_m_rt::{exception};
use rtic_monotonic::Monotonic;
use crate::systick::systick_rp2040::{busy_wait_millis, Systick};
use ringbuf::StaticRb;
use core::fmt::Write;
use critical_section::Mutex;
use embedded_hal::digital::v2::{OutputPin, StatefulOutputPin};
use crate::core2core_transfer::MessageToCore1;
use num_traits::{FromPrimitive, ToPrimitive};
use crate::core2core_transfer;
use crate::debug_print_macro;
use crate::drivers::shared_uart::Uart0Type;

pub(crate) static mut CORE1_STACK: rp_pico::hal::multicore::Stack<4096> =
    rp_pico::hal::multicore::Stack::new();


#[derive(Debug, PartialEq, Clone, Copy)]
enum State {
    Idle { last_status_check: fugit::Instant<u64, 1, 2_000> },
    IdleStatusCheck {},
    LoadProcessVar {},
    CheckProcessVarLoad { started: fugit::Instant<u64, 1, 2_000> },
    FSMError {},
    ProcessVarLoadTimeoutError {},
}

#[derive(Debug, Clone, Copy, PartialEq)]
enum Event {
    NoEvent,
    DeliverDoseRequest,
    StatusCheckRequest,
    LoadProcessVarRequest,
    ProcessVarLoadSuccess,
}


impl State {
    fn next(self,
            event: &Event,
            current_time: fugit::Instant<u64, 1, 2_000>,
            debug_uart: &Mutex<Uart0Type>) -> (State, bool) {
        let next_state = match (self, event) {
            // //////////////  State::Idle  //////////////
            (State::Idle { last_status_check }, Event::NoEvent) => {
                let dur_since_last_check = current_time - last_status_check;
                let dur_millis = dur_since_last_check.to_millis();
                if dur_millis > IDLE_STATUS_EVERY_MILLIS {
                    (State::IdleStatusCheck {}, true)
                } else {
                    (State::Idle { last_status_check }, true)
                }
            }
            (State::Idle { last_status_check: _last_status_check }, Event::StatusCheckRequest) => {
                (State::IdleStatusCheck {}, true)
            }
            (State::Idle { last_status_check: _last_status_check }, Event::LoadProcessVarRequest) => {
                (State::LoadProcessVar {}, true)
            }
            (State::Idle { last_status_check: last_status_check }, e) => {
                (State::Idle { last_status_check }, *e == Event::NoEvent)
            }

            // //////////////  State::IdleStatusCheck  //////////////
            (State::IdleStatusCheck {}, e) => {
                (State::Idle { last_status_check: current_time }, *e == Event::NoEvent)
            }

            // //////////////  State::LoadProcessVar  //////////////
            (State::LoadProcessVar {}, e) => {
                (State::CheckProcessVarLoad { started: current_time }, *e == Event::NoEvent)
            }

            // //////////////  State::CheckProcessVarLoad  //////////////

            (State::CheckProcessVarLoad { started: _started }, Event::ProcessVarLoadSuccess) => {
                (State::Idle { last_status_check: current_time }, true)
            }
            (State::CheckProcessVarLoad { started }, e) => {
                let dur_since_start_check = current_time - started;
                let dur_millis = dur_since_start_check.to_millis();
                if dur_millis > LOAD_PROCESS_VAR_TIMEOUT_MILLIS {
                    (State::ProcessVarLoadTimeoutError {}, *e == Event::NoEvent)
                } else {
                    (State::CheckProcessVarLoad { started }, *e == Event::NoEvent)
                }
            }

            // //////////////  State::ProcessVarLoadTimeoutError  //////////////

            (State::ProcessVarLoadTimeoutError {}, _e) => {
                (State::ProcessVarLoadTimeoutError {}, true)
            }

            // //////////////  State::Default case  //////////////

            (_s, _e) => {
                (State::FSMError {}, true)
            }
        };
        // debug_print_macro!(debug_uart, "[core1] State::Next() \
        //     \r\n    current_time = {:?} \
        //     \r\n    state        = {:?} \
        //     \r\n    next_state   = {:?}\r",
        // current_time, self, next_state);
        return next_state;
    }
}


static mut SYSTICK_CORE1: core::mem::MaybeUninit<Systick<2_000>> = core::mem::MaybeUninit::uninit();

#[exception]
fn SysTick() {
    let x: &mut Systick<2_000> = unsafe { crate::core1::SYSTICK_CORE1.assume_init_mut() };
    x.on_interrupt();
}

fn messageToEvent(message: MessageToCore1) -> Event {
    match message {
        core2core_transfer::MessageToCore1::RequestStatusCheck => { Event::StatusCheckRequest }
        core2core_transfer::MessageToCore1::RequestLoadProcessVar => { Event::LoadProcessVarRequest }
        core2core_transfer::MessageToCore1::RequestDeliverDose => { Event::DeliverDoseRequest }
    }
}


pub fn core1_task(mut tr: crate::core2core_transfer::TransferredToProcessCore) -> ! {
    let unsafe_pac = unsafe { rp_pico::pac::Peripherals::steal() };
    let core = unsafe { rp_pico::pac::CorePeripherals::steal() };
    let mut sio = rp_pico::hal::Sio::new(unsafe_pac.SIO);

    let debug_uart = tr.debug_uart;
    let adc = tr.adc;
    let mut gpio_boost_v_scaledown = tr.gpio_boost_v_scaledown;
    let mut gpio_boost_en = tr.gpio_boost_en;
    let mut i2c_process = tr.i2c_process;
    let vcontrol = crate::drivers::mcp4017::Mcp4017::new();
    critical_section::with(|cs| {
        debug_uart.borrow(cs).write_full_blocking(b"core1 sez: i am awake\r\n");
    });

    let vector_table = unsafe_pac.PPB.vtor.read().bits();
    // Here, we copy the vector table from core0 to be able to modify the systick handler


    let intercoresync = tr.intercore_sync;


    //  Monotonic rate 2000 Hz for 0.5 ms resolution on times
    let monotonic: &mut Systick<2000> = unsafe {
        crate::core1::SYSTICK_CORE1.write(Systick::new(tr.systick, 1_000_000))
    };
    unsafe {
        monotonic.reset();
    }

    let current_time = monotonic.now();

    debug_print_macro!(debug_uart, "[core1] ready to rumble!\r");

    // Get a few DCDC disabled reading as offset for the actual value
    let dcdc_zero_offset : f32 = crate::process::adc::read_adc_zero_offset_averaged(adc, monotonic,
    &mut gpio_boost_v_scaledown);

    debug_print_macro!(debug_uart, "[core1]: V DCDC zero-offset reading: (avg) {:6.3}\r",
    dcdc_zero_offset);

    debug_print_macro!(debug_uart, "[core1]: Set digipot! \r");
    vcontrol.set_target_voltage(i2c_process, 12.0);



    gpio_boost_en.set_high().unwrap();
    debug_print_macro!(debug_uart, "[core1] Enabled Boost DCDC\r");


    const RB_SIZE: usize = 5;
    let mut event_queue = StaticRb::<Event, RB_SIZE>::default();
    let (mut event_prod, mut event_cons) = event_queue.split_ref();

    let mut state = State::Idle { last_status_check: current_time };
    let mut event_consumed = false;

    let mut count = 0;

    loop {
        let current_time = monotonic.now();
        //debug_print_macro!(debug_uart, "[core1] current_time = {} ticks\r", current_time.ticks());

        //continue;

        match sio.fifo.read() {
            Some { 0: rawval } => {
                debug_print_macro!(debug_uart, "[core1] got rawval {:4}\r", rawval);

                let request: Option<MessageToCore1> = FromPrimitive::from_u32(rawval);
                match request {
                    Some { 0: message } => {
                        debug_print_macro!(debug_uart, "[core1] got a valid message, pushed event\r");
                        let event = messageToEvent(message);
                        event_prod.push(event).unwrap();
                    }
                    None {} => {
                        debug_print_macro!(debug_uart, "[core1] Failed to get a message from raw u32 val\r");
                    }
                }
            }
            None {} => {
                //debug_print_macro!(debug_uart, "[core1] FIFO empty ! \r");
            }
        }


        // Event queue push ordering has subtle implications if both are set
        // FIXME: use FIFO instead for intercor comms
        // if local_loadprocessvar {
        //     event_prod.push(Event::LoadProcessVarRequest).unwrap();
        // }
        // if local_triggered {
        //     event_prod.push(Event::TriggerRequest).unwrap();
        // }

        if event_cons.is_empty() {
            event_prod.push(Event::NoEvent).unwrap();
        }

        loop {
            let event = event_cons.iter().next();
            match event {
                Some { 0: e } => {
                    (state, event_consumed) = state.next(e, current_time, debug_uart);
                    if event_consumed {
                        event_cons.pop();
                    }
                }
                None => {
                    break;
                }
            }

            match state {
                State::Idle { last_status_check } => {
                    // Do the idle things
                    //debug_print_macro!(debug_uart, "[core1] Idle\r");
                }
                State::IdleStatusCheck {} => {

                    let voltage : f32 = crate::process::adc::read_dcdc_voltage(adc,
                                                                               &mut gpio_boost_v_scaledown,
                                                                               dcdc_zero_offset);

                    debug_print_macro!(debug_uart,
                        "[core1] DCDC Output voltage:  {:6.3}V\r",
                        voltage);


                    if gpio_boost_en.is_set_high().unwrap() {
                        count += 1;
                        if count > 10 {
                            count = 0;
                            gpio_boost_en.set_low().unwrap();
                        }
                    } else {
                        count += 1;
                        if count > 10 {
                            count = 0;
                            gpio_boost_en.set_high().unwrap();
                        }
                    }

                }
                _ => {}
            }
        }
    }
}
