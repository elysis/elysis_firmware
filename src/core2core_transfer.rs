use critical_section::Mutex;
use rp_pico::hal::{gpio, I2C};

use core::cell::UnsafeCell;
use crate::drivers::i2c_common::I2CBusProcess;

// critical_section is impl in the rp2040 hal with spinlock30 for real critical section
// At least, that is what I think it is doing. if facing strange race condition,
// just add a sprinkle of lock everywhere here and in the shared debug uart stuff :D
//type IntercoreSpinlock = rp_pico::hal::sio::Spinlock29;



pub struct TransferredToProcessCore {
    pub i2c_process: &'static mut I2CBusProcess,
    pub gpio_boost_en:  gpio::Pin<rp_pico::hal::gpio::bank0::Gpio6,
        rp_pico::hal::gpio::PushPullOutput>,
    pub gpio_boost_v_scaledown:  gpio::Pin<rp_pico::hal::gpio::bank0::Gpio27,
        rp_pico::hal::gpio::FloatingInput>,
    pub adc: &'static critical_section::Mutex<core::cell::RefCell<rp_pico::hal::Adc>>,
    pub intercore_sync: &'static IntercoreSync,
    pub systick: cortex_m::peripheral::SYST,
    pub debug_uart: &'static critical_section::Mutex<crate::drivers::shared_uart::Uart0Type>
}

pub static mut I2C_PROCESS_TBTRANSFERRED: core::mem::MaybeUninit<crate::core2core_transfer::I2CBusProcess> = core::mem::MaybeUninit::uninit();

pub static mut INTERCORE_SYNC: core::mem::MaybeUninit<IntercoreSync> = core::mem::MaybeUninit::uninit();

pub static mut DEBUG_UART_MUTEX:
core::mem::MaybeUninit<critical_section::Mutex<crate::drivers::shared_uart::Uart0Type>> = core::mem::MaybeUninit::uninit();

pub static mut ADC_MUTEX:
core::mem::MaybeUninit<critical_section::Mutex<
    core::cell::RefCell<rp_pico::hal::Adc>>> = core::mem::MaybeUninit::uninit();


#[derive(FromPrimitive, ToPrimitive)]
pub enum MessageToCore1 {
    RequestStatusCheck = 2,
    RequestLoadProcessVar,
    RequestDeliverDose
}

#[derive(FromPrimitive, ToPrimitive)]
pub enum MessageToCore0 {
    DoneStatusCheck = 2,
    DoneLoadProcessVar,
    DoneDeliverDose
}

#[derive(Clone, Copy)]
pub struct ProcessVars {
    dose_uc: f32,
    current_ua: f32,
    max_volt: f32,
    abort_on_fail: bool
}

impl ProcessVars {
    pub fn new() -> ProcessVars {
        return ProcessVars {
            dose_uc: 100.0,
            current_ua: 100.0,
            max_volt: 7.0,
            abort_on_fail: true
        };
    }
}

struct IntercoreSyncInternal {
    processvars: ProcessVars
}

pub struct IntercoreSync {
    internal: Mutex<UnsafeCell<IntercoreSyncInternal>>,
}

impl IntercoreSyncInternal {
    pub fn new() -> IntercoreSyncInternal {
        return IntercoreSyncInternal {
            processvars: ProcessVars::new()
        };
    }
}

impl IntercoreSync {
    pub fn new() -> IntercoreSync {
        return IntercoreSync {
            internal: Mutex::new(UnsafeCell::new(IntercoreSyncInternal::new())),
        };
    }

    pub fn set_processvar(&self, pvars: ProcessVars)   {
        critical_section::with(|cs| {

            let internal_borrow
                = self.internal.borrow(cs).get();

            unsafe {
                (*internal_borrow).processvars = pvars;
            }
        });
    }

    pub fn get_processvar(&self,) -> ProcessVars   {
        let mut pvars: ProcessVars = ProcessVars::new();
        critical_section::with(|cs| {

            let internal_borrow
                = self.internal.borrow(cs).get();

            unsafe {
                pvars= (*internal_borrow).processvars;
            }
        });
        return pvars;
    }

}



#[macro_export]
macro_rules! debug_print_macro {
    ($debug_uart:expr, $($arg:tt)*) => {
        if(DO_DEBUG_PRINTS) {
        critical_section::with(|cs| {
            let mut uart_wrapper = crate::drivers::shared_uart::UartWriteWrapper {
                0: $debug_uart.borrow(cs)
            };
            writeln!(uart_wrapper, $($arg)*).unwrap();
        });
    }
    }
}