use core::fmt::Write;
use rp_pico::hal::{gpio, I2C};

pub fn serial_write_i2c_error(err: rp_pico::hal::i2c::Error,
                              uart: &'static critical_section::Mutex< crate::drivers::shared_uart::Uart0Type>) {

    critical_section::with(|cs| {
        let mut uart_wrapper = crate::drivers::shared_uart::UartWriteWrapper{ 0: uart.borrow(cs)};

        match err {
            rp_pico::hal::i2c::Error::Abort(e) => {
                writeln!(uart_wrapper, "i2c_aux: Error Abort {:b}\r", e).unwrap();
            }
            rp_pico::hal::i2c::Error::InvalidReadBufferLength => {
                writeln!(uart_wrapper, "i2c_aux: Error InvalidReadBufferLength\r",).unwrap();
            }
            rp_pico::hal::i2c::Error::InvalidWriteBufferLength => {
                writeln!(uart_wrapper, "i2c_aux: Error InvalidWriteBufferLength\r",).unwrap();
            }
            rp_pico::hal::i2c::Error::AddressOutOfRange(a) => {
                writeln!(uart_wrapper, "i2c_aux: Error AddressOutOfRange {}\r", a).unwrap();
            }
            rp_pico::hal::i2c::Error::AddressReserved(a) => {
                writeln!(uart_wrapper, "i2c_aux: Error AddressReserved {}\r", a).unwrap();
            }
            _ => {
                writeln!(uart_wrapper, "i2c_aux: Other error\r").unwrap();
            }
        }
    });


}

pub type I2CBusAux = I2C<
    rp_pico::pac::I2C0,
    (
        gpio::Pin<rp_pico::hal::gpio::bank0::Gpio12, gpio::FunctionI2C>,
        gpio::Pin<rp_pico::hal::gpio::bank0::Gpio13, gpio::FunctionI2C>,
    ),
>;

pub type I2CBusProcess = I2C<
    rp_pico::pac::I2C1,
    (
        gpio::Pin<rp_pico::hal::gpio::bank0::Gpio14, gpio::FunctionI2C>,
        gpio::Pin<rp_pico::hal::gpio::bank0::Gpio15, gpio::FunctionI2C>,
    ),
>;
