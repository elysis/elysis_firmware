
static GAMMA_LUT: &'static [u8] = &[
0,
0,
0,
1,
2,
4,
6,
8,
11,
15,
19,
24,
29,
35,
41,
48,
56,
64,
73,
83,
93,
104,
116,
128,
142,
155,
170,
186,
202,
219,
236,
255,
];

pub fn bright2pwm(bright: u8) -> u8 {
    if bright >= GAMMA_LUT.len() as u8 {
        let res = GAMMA_LUT[GAMMA_LUT.len() - 1];
        return res;
    }
    let res = GAMMA_LUT[bright as usize];
    return res;
}
