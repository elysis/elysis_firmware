use crate::drivers::i2c_common::I2CBusProcess;
use embedded_hal::prelude::*;
use rp_pico::hal::i2c::Error;

const MCP4017_ADDR: u8 = 0b0101111;

const MCP4017_WIPER_R: f32 = 100.0;

const MCP4017_RESISTOR_INCREMENT: f32 = 5000.0/127.0;

// Regulated voltage at the FB pin of the DCDC switcher IC
const TLV6104_FBVOLTAGE: f32 = 0.795;

const DCDC_FB_R1: f32 = 40_000.0;

pub struct Mcp4017 {

}

impl Mcp4017 {
    pub fn new() -> Mcp4017 {
        return Mcp4017 {};
    }

    pub fn set_resistor_reg(&self, i2c: &mut I2CBusProcess,
                        regval: u8)-> Result<(), Error> {
        // There is a single reg, the 7-bit wiper reg (most sig bit ignored)
        i2c.write(MCP4017_ADDR, &[regval])
    }

    pub fn compute_resistance_for_voltage(&self, voltage: f32) -> f32 {
        let fb_over_v: f32 = TLV6104_FBVOLTAGE/voltage;
        let r2 = (fb_over_v *DCDC_FB_R1)/(1.0- fb_over_v);
        let mut digitpot_r = r2 - 1400.0; // Already have 1.4k R fixed resistance in r2

        if digitpot_r < 0.0 {
            digitpot_r = 0.0;
        }

        return digitpot_r;
    }

    pub fn set_target_resistance(&self, i2c: &mut I2CBusProcess,
                              resistance: f32) -> Result<(), Error>{
        let mut eff_r = resistance - MCP4017_WIPER_R;

        // Clamp resistance:
        if eff_r > 5000.0 {
            eff_r = 5000.0;
        }

        if eff_r < 0.0 {
            eff_r = 0.0;
        }

        let count_float = eff_r / MCP4017_RESISTOR_INCREMENT;
        let count = count_float as u8;
        i2c.write(MCP4017_ADDR, &[count])
    }

    pub fn set_target_voltage(&self, i2c: &mut I2CBusProcess,
                              voltage: f32) -> Result<(), Error>{
        let r2 = self.compute_resistance_for_voltage(voltage);
        self.set_target_resistance(i2c, r2)
    }

}