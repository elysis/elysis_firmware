use crate::drivers::i2c_common::I2CBusAux;
use embedded_hal::prelude::*;
use rp_pico::hal::i2c::Error;

const PCA9635_ADDR: u8 = 0b0001011;

pub struct Pca9635 {

}

#[derive(Default)]
pub struct StatusLedsPWM {
    pub f1: u8,
    pub f2: u8,

    pub dose_1: u8,
    pub dose_2: u8,
    pub dose_3: u8,
    pub dose_4: u8,
    pub dose_5: u8,
    pub dose_6: u8,
    pub dose_supp: u8,

    pub maxv_1: u8,
    pub maxv_2: u8,
    pub maxv_3: u8,

    pub microamp_1: u8,
    pub microamp_2: u8,
    pub microamp_3: u8,
    pub microamp_supp: u8,
}

impl StatusLedsPWM {
    pub fn new() -> StatusLedsPWM {
        return StatusLedsPWM {..Default::default()
        };
    }
}

impl Pca9635 {

    pub fn new() -> Pca9635 {
        return Pca9635 {};
    }

    pub fn initial_setup(&self, i2c:&mut I2CBusAux) -> Result<(), Error> {
        // Set MODE0 and MODE1 reg to sensible values for the hw

        i2c.write(PCA9635_ADDR, &[0b100_00000,
            0b100_0_0001, // autoincr, normal mode, respond to all call
            0b00001_0_01 //  group control = dimming, output logic not inverted
                         //  update on ACK, open drain driver, nOE -> high impedance
        ])?;

        i2c.write(PCA9635_ADDR, &[0b100_10010,
            0xFF, 0xFF, // Group PWM, Group freq maximum
            0xFF,0xFF,0xFF,0xFF //  LED out controlled through PWM and Group settings
        ])
    }

    pub fn set_led(&self, i2c:&mut I2CBusAux, ledstatuspwm: StatusLedsPWM) -> Result<(), Error> {

        i2c.write(PCA9635_ADDR, &[0b100_00010,
            ledstatuspwm.f1,
            ledstatuspwm.f2,
            ledstatuspwm.dose_1,
            ledstatuspwm.dose_2,
            ledstatuspwm.dose_3,
            ledstatuspwm.dose_4,
            ledstatuspwm.dose_5,
            ledstatuspwm.dose_6,
            ledstatuspwm.dose_supp,
            ledstatuspwm.maxv_1,
            ledstatuspwm.maxv_2,
            ledstatuspwm.maxv_3,
            ledstatuspwm.microamp_1,
            ledstatuspwm.microamp_2,
            ledstatuspwm.microamp_3,
            ledstatuspwm.microamp_supp
        ])
    }

    pub fn set_global_pwm(&self, i2c:&mut I2CBusAux, pwm: u8) -> Result<(), Error> {
        i2c.write(PCA9635_ADDR, &[0b100_10010,
            pwm
        ])
    }

    pub fn get_global_pwm(&self, i2c:&mut I2CBusAux) -> u8 {
        let mut res_buff : [u8; 1] = [0x00];
        i2c.write_read(PCA9635_ADDR, &[0b100_10010], &mut res_buff).unwrap();
        return res_buff[0];
    }
}