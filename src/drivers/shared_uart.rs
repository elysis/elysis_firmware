use core::fmt::{self, Write};


pub type Uart0Type = rp_pico::hal::uart::UartPeripheral<rp_pico::hal::uart::Enabled,
    rp_pico::pac::UART0, (rp_pico::hal::gpio::Pin<rp_pico::hal::gpio::bank0::Gpio0,
        rp_pico::hal::gpio::Function<rp_pico::hal::gpio::Uart>>,
            rp_pico::hal::gpio::Pin<rp_pico::hal::gpio::bank0::Gpio1,
                rp_pico::hal::gpio::Function<rp_pico::hal::gpio::Uart>>)>;


pub struct UartWriteWrapper<'a>(pub(crate) &'a Uart0Type);

impl Write for UartWriteWrapper<'_> {
     fn write_str(&mut self, s: &str) -> fmt::Result {
        self.0.write_full_blocking(s.as_bytes());
        Ok(())
    }
}
