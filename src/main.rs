#![no_std]
#![no_main]
#![feature(error_in_core)]

#[macro_use]
extern crate num_derive;

mod core1;
mod core2core_transfer;
mod drivers {
    pub(crate) mod pca9635;
    pub(crate) mod i2c_common;
    pub(crate) mod led_common;
    pub(crate) mod shared_uart;
    pub(crate) mod mcp4017;
}
mod process {
    pub(crate) mod adc;
}
mod systick {
    pub(crate) mod systick_rp2040;
}
mod params;

use panic_halt as _;



#[rtic::app(
device = rp_pico::hal::pac,
dispatchers = [TIMER_IRQ_1]
)]
mod app {
    use rp2040_monotonic::{
        fugit::Duration,
        fugit::RateExtU32, // For .kHz() conversion funcs
        Rp2040Monotonic,
    };
    use rp_pico::hal::{clocks, gpio, gpio::pin::bank0::{Gpio25},
                       gpio::pin::PushPullOutput, sio::Sio, watchdog::Watchdog, I2C, Clock};
    use rp_pico::XOSC_CRYSTAL_FREQ;
    use embedded_hal::digital::v2::{OutputPin, ToggleableOutputPin};
    use rp_pico::hal::gpio::{FunctionUart, OutputDriveStrength};
    use rp_pico::hal::uart::{StopBits, UartConfig, UartPeripheral};
    use crate::core2core_transfer::IntercoreSync;
    use crate::drivers::led_common::bright2pwm;
    use crate::drivers::pca9635::Pca9635;
    use embedded_hal::adc::OneShot;
    use core::fmt::Write;
    use embedded_hal::prelude::_embedded_hal_blocking_i2c_Write;
    use num_traits::FromPrimitive;
    use crate::core2core_transfer::MessageToCore1;
    use crate::debug_print_macro;
    use crate::params::*;

    const MONO_NUM: u32 = 1;
    const MONO_DENOM: u32 = 1000000;
    const ONE_SEC_TICKS: u64 = 1000000;
    const ONE_MILLICSEC_TICKS: u64 = 1000;


    #[monotonic(binds = TIMER_IRQ_0, default = true)]
    type Rp2040Mono = Rp2040Monotonic;

    //#[monotonic(binds = SysTick, default = true)]
    // 2000 hz = 0.5 ms resolution ought to be enough for anybody
    //type Rp2040Mono =  dwt_systick_monotonic::DwtSystick<2_000>;


    #[shared]
    struct Shared {
        debug_uart: &'static critical_section::Mutex< crate::drivers::shared_uart::Uart0Type>,
    }

    #[local]
    struct Local {
        led: gpio::Pin<Gpio25, PushPullOutput>,
        i2c_aux: crate::drivers::i2c_common::I2CBusAux,
        ledpwm: Pca9635
    }

    #[init(local=[
    // Task local initialized resources are static
    // Here we use MaybeUninit to allow for initialization in init()
    // This enables its usage in driver initialization
    //led
    //MaybeUninit<crate::drivers::i2c_aux_bus::I2CBusAux> = MaybeUninit::uninit()

    ])]
    fn init(mut ctx: init::Context) -> (Shared, Local, init::Monotonics) {

        // /////////////////////// General setup for rp2040 /////////////////////////////

        // Configure the clocks, watchdog - The default is to generate a 125 MHz system clock
        let mut watchdog = Watchdog::new(ctx.device.WATCHDOG);
        let clocks = clocks::init_clocks_and_plls(
            XOSC_CRYSTAL_FREQ,
            ctx.device.XOSC,
            ctx.device.CLOCKS,
            ctx.device.PLL_SYS,
            ctx.device.PLL_USB,
            &mut ctx.device.RESETS,
            &mut watchdog,
        )
            .ok()
            .unwrap();

        //let mut pac = pac::Peripherals::take().unwrap();
        let mut sio = Sio::new(ctx.device.SIO);


        // /////////////////////// Prepare various gpio /////////////////////////////

        let gpioa = rp_pico::Pins::new(
            ctx.device.IO_BANK0,
            ctx.device.PADS_BANK0,
            sio.gpio_bank0,
            &mut ctx.device.RESETS,
        );

        let mut led = gpioa.led.into_push_pull_output();
        led.set_low().unwrap();

        // Init Boost DCDC enable pin
        let mut boost_en = gpioa.gpio6.into_push_pull_output();
        boost_en.set_low().unwrap();

        let mut switch_1 = gpioa.gpio7.into_floating_input();
        let mut switch_2 = gpioa.gpio8.into_floating_input();
        let mut switch_3 = gpioa.gpio28.into_floating_input();

        let mut gpio_boost_v_scaledown = gpioa.gpio27.into_floating_input();

        // ///////////////////////////// Prepare debug uart ////////////////////////////////////
        let pins = (
            gpioa.gpio0.into_mode::<FunctionUart>(),
            gpioa.gpio1.into_mode::<FunctionUart>(),
        );

        let uart: &'static _ = unsafe {
            crate::core2core_transfer::DEBUG_UART_MUTEX.write( critical_section::Mutex::new(
                UartPeripheral::new(ctx.device.UART0, pins,
                                    &mut ctx.device.RESETS).enable(
                    UartConfig::new(115200.Hz(),
                                    rp_pico::hal::uart::DataBits::Eight,
                                    None, StopBits::One),
                    clocks.peripheral_clock.freq(),
                ).unwrap()

            ))};

        critical_section::with(|cs| {
            uart.borrow(cs).write_full_blocking(b"\n\n\n\n");
        });
        debug_print_macro!(uart, "\n\n\n[core0]: UART ready\r");


        // ///////////////////////////// Prepare ADC ////////////////////////////////////

        let mut adc: &'static _ = unsafe {
            crate::core2core_transfer::ADC_MUTEX.write( critical_section::Mutex::new(
                core::cell::RefCell::new(
                rp_pico::hal::Adc::new(ctx.device.ADC, &mut ctx.device.RESETS))
        ))};


        let reading_v_cc_scaledown: u16 = critical_section::with(|cs| {
            adc.borrow(cs).borrow_mut().read(&mut  gpio_boost_v_scaledown).unwrap()
        });

        let mut temperature_sensor = critical_section::with(|cs| {
            adc.borrow(cs).borrow_mut().enable_temp_sensor()
        });

        let reading_temperature: u16 = critical_section::with(|cs| {
            adc.borrow(cs).borrow_mut().read(&mut temperature_sensor).unwrap()
        });

        debug_print_macro!(uart, "[core0]: ADC ready\r");
        debug_print_macro!(uart, "[core0]: ADC V_CC_scaledown {:6} TEMP {:6} \r", reading_v_cc_scaledown, reading_temperature);



        // ///////////////////////////// Prepare i2c process bus ////////////////////////////////////
        let i2c_process_sda = gpioa.gpio14.into_mode::<gpio::FunctionI2C>();
        let i2c_process_scl = gpioa.gpio15.into_mode::<gpio::FunctionI2C>();

        let i2c_process_tbtransferred: &'static mut _ = unsafe {
            crate::core2core_transfer::I2C_PROCESS_TBTRANSFERRED.write( I2C::i2c1(
            ctx.device.I2C1,
            i2c_process_sda,
            i2c_process_scl,
            100.kHz(), // Fast mode i2c require special handling
            &mut ctx.device.RESETS,
            &clocks.system_clock,
        ))};


        debug_print_macro!(uart, "[core0]: i2c process bus ready\r");


        // ///////////////////////////// Prepare i2c aux bus ////////////////////////////////////
        let mut i2c_aux_sda = gpioa.gpio12.into_mode::<gpio::FunctionI2C>();
        let mut i2c_aux_scl = gpioa.gpio13.into_mode::<gpio::FunctionI2C>();

        i2c_aux_sda.set_drive_strength(OutputDriveStrength::TwelveMilliAmps);
        i2c_aux_scl.set_drive_strength(OutputDriveStrength::TwelveMilliAmps);


        let mut i2c_aux: crate::drivers::i2c_common::I2CBusAux = I2C::i2c0(
            ctx.device.I2C0,
            i2c_aux_sda,
            i2c_aux_scl,
            10.kHz(), // Fast mode i2c require special handling
            &mut ctx.device.RESETS,
            &clocks.system_clock,
        );

        debug_print_macro!(uart, "[core0]: i2c aux bus ready\r");


        let led_pwm = Pca9635::new();
        let _ = led_pwm.initial_setup(&mut i2c_aux);

        debug_print_macro!(uart, "[core0]: Pca9635 ready\r");


        // /////////////////////// Setup systick struct ////////////////////////////////////

        // Other core will use systick for timing
        let systick : cortex_m::peripheral::SYST  = ctx.core.SYST;

        // /////////////////////// Transfer and launch core1 ////////////////////////////////////

        // Prepare the message passing struct
        let intercoresync: &'static _ = unsafe {
            crate::core2core_transfer::INTERCORE_SYNC.write( IntercoreSync::new())
        };

        // Structure that will be passed to core1
        let tr = crate::core2core_transfer::TransferredToProcessCore {
            i2c_process: i2c_process_tbtransferred,
            gpio_boost_en: boost_en,
            gpio_boost_v_scaledown: gpio_boost_v_scaledown,
            adc: adc,
            intercore_sync: intercoresync,
            systick: systick,
            debug_uart: uart
        };



        // Launch the second core process
        let mut mc = rp_pico::hal::multicore::Multicore::new(&mut ctx.device.PSM,
                                                             &mut ctx.device.PPB, &mut sio.fifo);
        let cores = mc.cores();
        let core1 = &mut cores[1];
        let _test = core1.spawn(unsafe { &mut crate::core1::CORE1_STACK.mem },
                                // Lambda with move capture for tr struct
                                move || {
                                    crate::core1::core1_task(tr)
                                }
                                );


        debug_print_macro!(uart, "[core0]: Launched core1\r");



        debug_print_macro!(uart, "[core0]: Just before FIFO write\r");


        let request = MessageToCore1::RequestStatusCheck as u32;


        sio.fifo.write(request);

        debug_print_macro!(uart, "[core0]: Just after FIFO write\r");


        let request =  MessageToCore1::RequestStatusCheck as u32;
        sio.fifo.write(request);

        debug_print_macro!(uart, "[core0]: Just after another write\r");


        // /////////////////////// Launch task and finalize RTIC setup /////////////////////////////

        let mono = Rp2040Mono::new(ctx.device.TIMER);

        // Default systick clock is the 1us signal from watchdog
        //let mono : Rp2040Mono = Rp2040Mono::new(ctx.device.DCB, ctx.device.DWT, ctx.device.SYST, 1_000_000);


        // Spawn heartbeat task
        heartbeat::spawn().unwrap();
        update_leds_task::spawn().unwrap();

        return (
            Shared {
                debug_uart: uart,
            },
            Local { led: led,
                i2c_aux: i2c_aux,
                ledpwm: led_pwm
            },
            init::Monotonics(mono),
        )
    }

    #[task(local = [led])]
    fn heartbeat(ctx: heartbeat::Context) {
        // Flicker the built-in LED
        _ = ctx.local.led.toggle();


        // Re-spawn this task after 1 second
        let one_second = Duration::<u64, MONO_NUM, MONO_DENOM>::from_ticks(ONE_SEC_TICKS);
        heartbeat::spawn_after(one_second).unwrap();
    }



    #[task(local = [i2c_aux, ledpwm], shared=[debug_uart])]
    fn update_leds_task(mut ctx: update_leds_task::Context) {


        ctx.shared.debug_uart.lock(|&mut debug_uart| {
            //debug_print_macro!(debug_uart, "[core0]: i2c_aux: Trying to contact the PWM controller\r");
        });


        let mut statusledpwm = crate::drivers::pca9635::StatusLedsPWM::new();
        statusledpwm.dose_1 = bright2pwm(5);
        statusledpwm.dose_2 = bright2pwm(5);
        statusledpwm.dose_3 = bright2pwm(5);
        statusledpwm.dose_4 = bright2pwm(5);
        statusledpwm.dose_5 = bright2pwm(32);
        statusledpwm.dose_supp = bright2pwm(0);

        let res = ctx.local.ledpwm.set_led(ctx.local.i2c_aux, statusledpwm);

        let g_pwm = ctx.local.ledpwm.get_global_pwm(ctx.local.i2c_aux);
        if g_pwm != bright2pwm(5) {
            let _ = ctx.local.ledpwm.set_global_pwm(ctx.local.i2c_aux, bright2pwm(5));
        } else {
            let _ = ctx.local.ledpwm.set_global_pwm(ctx.local.i2c_aux, bright2pwm(10));
        }


        ctx.shared.debug_uart.lock(|debug_uart| {


                match res {
                    core::prelude::v1::Err(errcontent) => {
                        crate::drivers::i2c_common::serial_write_i2c_error(errcontent,
                                                                           debug_uart)
                    }
                    core::prelude::v1::Ok(()) => {
                    }
                }


        });


        let ms3000 = Duration::<u64, MONO_NUM, MONO_DENOM>::from_ticks(ONE_MILLICSEC_TICKS * 1000);
        update_leds_task::spawn_after(ms3000).unwrap();
    }
}



