

pub const IDLE_STATUS_EVERY_MILLIS: u64 = 500;
pub const LOAD_PROCESS_VAR_TIMEOUT_MILLIS: u64 = 500;

pub const DO_DEBUG_PRINTS : bool = true;

// Use this to device-dependently tune the ADC readout (applied on the final reported voltage)
pub const DCDC_MONITOR_CALIBRATION_OFFSET : f32 = -0.12;
