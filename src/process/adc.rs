use cortex_m::prelude::*;
use rp_pico::hal::{gpio};
use crate::params::DCDC_MONITOR_CALIBRATION_OFFSET;

const MAX_ADC_COUNT_AS_F32: f32 = 0xFFF as f32;
const ADC_REF_VOLTAGE: f32 = 3.3;
const V_BOOST_SCALEUP_FACTOR: f32 = 10.090909;

pub fn read_adc_zero_offset_averaged(adc: &critical_section::Mutex<core::cell::RefCell<rp_pico::hal::Adc>>,
                           monotonic: &mut crate::systick::systick_rp2040::Systick<2000>,
                           gpio_boost_v_scaledown: &mut gpio::Pin<rp_pico::hal::gpio::bank0::Gpio27,
                               rp_pico::hal::gpio::FloatingInput>) -> f32 {
    let dcdc_zero_offset_count_1: u16 = critical_section::with(|cs| {
        adc.borrow(cs).borrow_mut().read(gpio_boost_v_scaledown).unwrap()
    });
    let dcdc_zero_offset_read_1 =
        ((dcdc_zero_offset_count_1 as f32) / MAX_ADC_COUNT_AS_F32) * ADC_REF_VOLTAGE;

    crate::systick::systick_rp2040::busy_wait_millis(monotonic, 100);

    let dcdc_zero_offset_count_2: u16 = critical_section::with(|cs| {
        adc.borrow(cs).borrow_mut().read(gpio_boost_v_scaledown).unwrap()
    });
    let dcdc_zero_offset_read_2 =
        ((dcdc_zero_offset_count_2 as f32) / MAX_ADC_COUNT_AS_F32) * ADC_REF_VOLTAGE;

    crate::systick::systick_rp2040::busy_wait_millis(monotonic, 100);

    let dcdc_zero_offset_count_3: u16 = critical_section::with(|cs| {
        adc.borrow(cs).borrow_mut().read(gpio_boost_v_scaledown).unwrap()
    });
    let dcdc_zero_offset_read_3 =
        ((dcdc_zero_offset_count_3 as f32) / MAX_ADC_COUNT_AS_F32) * ADC_REF_VOLTAGE;

    let dcdc_zero_offset: f32 = (dcdc_zero_offset_read_1
        + dcdc_zero_offset_read_2
        + dcdc_zero_offset_read_3) / 3.0_f32;

    return dcdc_zero_offset;
}

pub fn read_dcdc_voltage(adc: &critical_section::Mutex<core::cell::RefCell<rp_pico::hal::Adc>>,
                         gpio_boost_v_scaledown: &mut gpio::Pin<rp_pico::hal::gpio::bank0::Gpio27,
                             rp_pico::hal::gpio::FloatingInput>,
                         dcdc_zero_offset: f32) -> f32 {
    let reading_v_cc_scaledown: u16 = critical_section::with(|cs| {
        adc.borrow(cs).borrow_mut().read(gpio_boost_v_scaledown).unwrap()
    });

    let voltage_scaleddown =
        ((reading_v_cc_scaledown as f32) / MAX_ADC_COUNT_AS_F32) * ADC_REF_VOLTAGE;
    let voltage: f32 = (voltage_scaleddown * V_BOOST_SCALEUP_FACTOR) - dcdc_zero_offset
        + DCDC_MONITOR_CALIBRATION_OFFSET;

    return voltage;
}